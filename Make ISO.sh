#!/bin/bash

#Ok, I made some scripts to make LiveCDTmp easier to do post-filesystem building, the other script also fixes a bug that causes the system to be non-installable due to excluding '/boot' from the filesystem file. Anyways, here's what you have to do:
#Hit Ctrl+H in Gedit to access Find And Replace, and do the following:
#Replace 'LiveCDTmpFolderLocation' with the location of where you placed 'livecdtmp' earlier
#Replace 'DistroName' with whatever you want the disc/ISO to identify itself as
#Replace 'FinishedISOOutputFile' with the location you want the created ISO to be saved
#and Replace 'username' with your lower case, no spaces user name. 
#(Tip: Copy the folder and paste it in the window, and it will paste the location of the folder)

# $1 = Your username

#Checks and makes sure the dependencies are installed before proceeding with the script:
apt-get install dumpet xorriso squashfs-tools gddrescue isolinux -y

cd "LiveCDTmpFolderLocation"

#Deletes and creates a new filesystem.squashfs file with the new contents of "edit":
rm -rf "./extract-cd/casper/filesystem.squashfs"

#The 1048576 is the blocksize. The higher it is, the more compressed the SquashFS. You can choose to leave it like this, and have a SLOW SquashFS Process when squashing it, or you can make it a smaller number for a faster process, to the sacrifice of the SquashFS being closer to the actual contents of the file in total size.
mksquashfs "./edit" "./extract-cd/casper/filesystem.squashfs" -b 1048576 -comp xz -always-use-fragments

#Get the size of the filesystem, this is a step in LiveCDCustomisation:
printf $(du -sx --block-size=1 "LiveCDTmpFolderLocation/edit" | cut -f1) > "LiveCDTmpFolderLocation/extract-cd/casper/filesystem.size"

#Delete and make the file that holds the MD5Sums of the contents of the ISO
rm "./extract-cd/md5sum.txt"
cd "./extract-cd"

#MD5Sum Generation
find -type f -print0 | xargs -0 md5sum | grep -v isolinux/boot.cat | tee md5sum.txt

#Extract the ISOHYBRID MBR from the Base Distro, we'll need this later for ISOHYBRID-ising the EFI-compatible ISO as it's made
dd if=../BaseDistro.iso bs=512 count=1 of=./isolinux/isohdpfx.bin

#Delete the old and make the new ISO file
rm -rf "FinishedISOOutputFile"

#This command is completely different to the one you'd expect to see on the Ubuntu Wiki Article, this is because unlike 'mkisofs' on its own, this one, with this command format, allows you to make an EFI/Legacy Bootable ISO, and not just a Legacy Bootable ISO.
xorriso -as mkisofs -isohybrid-mbr isolinux/isohdpfx.bin \
-V "DistroName" -c isolinux/boot.cat -b isolinux/isolinux.bin -no-emul-boot -boot-load-size 4 \
-boot-info-table -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot \
-isohybrid-gpt-basdat -o "FinishedISOOutputFile" .

#Change Permissions of the ISO file and its owner, remember: you made this ISO with the root user account!
chmod 755 "FinishedISOOutputFile"
chown -h $1:$1 "FinishedISOOutputFile"

#DO NOT use ISOHYBRID on the finished ISO, from my attempts at trying that, it's ruined the EFI Bootability. Also, the ISO Making command ISOHYBRID-ises the ISO anyway, without breaking EFI Support.